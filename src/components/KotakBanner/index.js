import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import React from 'react';
import {Dimensions} from 'react-native';
import {Banner} from '../../assets';
import ButtonIcon from '../ButtonIcon';

const Saldo = () => {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={Banner}
        imageStyle={{borderRadius: 12}}
        style={styles.banner}>
        <View style={styles.text}>
          <Text style={styles.kalimatAtas}>Sewa Mobil Berkualitas</Text>
        </View>
        <View style={styles.text}>
          <Text style={styles.kalimatBawah}>di kawasanmu</Text>
        </View>
        <View style={styles.buttonAksi}>
          <ButtonIcon title=" " />
        </View>
      </ImageBackground>
    </View>
  );
};

export default Saldo;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    marginHorizontal: 20,
    borderRadius: 12,
    marginTop: -windowHeight * 0.09,
    width: windowWidth * 0.9,
  },
  text: {
    flexDirection: 'row',
  },
  kalimatAtas: {
    paddingTop: 17,
    paddingLeft: 17,
    fontSize: 18,
    color: 'white',
  },
  kalimatBawah: {
    paddingLeft: 17,
    fontSize: 18,
    color: 'white',
  },
  banner: {
    width: windowWidth * 0.9,
    height: 130,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  buttonAksi: {
    paddingTop: 17,
    paddingLeft: 17,
    paddingBottom: 17,
  },
});
