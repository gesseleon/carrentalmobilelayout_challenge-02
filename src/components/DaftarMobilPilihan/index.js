import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  Image,
} from 'react-native';
import React from 'react';
import {Xenia} from '../../assets';
import {IconUser, IconBriefcase} from '../../assets';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const DaftarMobilPilihan = () => {
  return (
    <TouchableOpacity style={styles.container}>
      <ImageBackground source={Xenia} style={styles.xenia}></ImageBackground>

      <View>
        <Text
          style={{
            textAlign: 'left',
            fontWeight: '400',
            color: 'black',
            fontSize: 15,
            paddingLeft: 8,
          }}>
          {' '}
          Daihatsu Xenia
        </Text>

        <View style={styles.icon}>
          <View style={styles.icon1}>
            <IconUser />
            <Text style={styles.spacing1}>4</Text>
            <IconBriefcase style={styles.spacing} />
            <Text style={styles.spacing1}>2</Text>
          </View>
        </View>

        <View style={styles.harga}>
          <Text
            style={{
              textAlign: 'left',
              fontWeight: '400',
              color: 'black',
              fontSize: 15,
              color: 'green',
              paddingLeft: 8,
            }}>
            {' '}
            Rp. 230.000
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default DaftarMobilPilihan;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    padding: 22,
    backgroundColor: 'white',
    flexDirection: 'row',
    width: windowWidth * 0.9,
    height: 98,
    marginLeft: 20,
    elevation: 2,

    marginVertical: windowHeight * 0.02,
    alignItems: 'center',
  },

  icon: {
    flexDirection: 'row',
    marginTop: 5,
  },

  icon1: {
    marginLeft: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  spacing: {
    marginLeft: 10,
  },
  spacing1: {
    marginLeft: 5,
  },
  harga: {
    marginTop: 7,
  },

  xenia: {
    height: 70,
    width: 110,
  },
});
