import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {Akun} from '../../assets';

const AkunPage = () => {
  return (
    <View style={styles.page}>
      <Text style={styles.top}>Akun</Text>
      <View style={{alignItems: 'center'}}>
        <Image source={Akun} style={styles.akun} />
        <Text style={styles.font}>
          Upsss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <TouchableOpacity>
          <Text style={styles.fontRegister}>Register</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={{color: 'white'}}>.</Text>
      </View>
    </View>
  );
};

export default AkunPage;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  top: {
    marginLeft: 30,
    fontSize: 18,
    color: 'black',
    fontWeight: '700',
    marginTop: 30,
    alignSelf: 'flex-start',
  },
  akun: {
    width: windowWidth,
    height: 100,
  },
  font: {
    fontSize: 16,
    margin: 20,
    color: 'black',
    textAlign: 'center',
  },
  fontRegister: {
    textAlign: 'center',
    backgroundColor: '#5CB85F',
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
});
