import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  StatusBar,
  Image,
} from 'react-native';
import React from 'react';
import {ImageHeader} from '../../assets';
import {Dimensions} from 'react-native';
import {WARNA_DISABLE} from '../../utils/constant';
import {KotakBanner} from '../../components/';
import ButtonIcon from '../../components/ButtonIcon';
import DaftarMobilPilihan from '../../components/DaftarMobilPilihan';
import {Foto} from '../../assets';

const Home = () => {
  return (
    <View style={styles.page}>
      <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <View style={styles.container}>
            <View>
              <Text style={styles.username}>Hi, Gessel Leon</Text>
              <Text style={styles.location}>Medan, Indonesia</Text>
            </View>

            <Image source={Foto} style={styles.foto}></Image>
          </View>
        </ImageBackground>

        <KotakBanner />

        <View style={styles.layanan}>
          <View style={styles.iconLayanan}>
            <ButtonIcon title="Sewa Mobil" type="Layanan" />
            <ButtonIcon title="Oleh-Oleh" type="Layanan" />
            <ButtonIcon title="Penginapan" type="Layanan" />
            <ButtonIcon title="Wisata" type="Layanan" />
          </View>

          <View style={styles.mobilpilihan}>
            <Text
              style={{
                marginLeft: 30,
                fontSize: 18,
                color: 'black',
                fontWeight: 'bold',
                marginTop: 30,
              }}>
              Daftar Mobil Pilihan
            </Text>
          </View>
        </View>
        <DaftarMobilPilihan />
        <DaftarMobilPilihan />
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.25,
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 50,
    width: windowWidth * 0.9,
  },
  foto: {
    height: 40,
    width: 40,
  },
  username: {
    fontSize: 16,
    color: WARNA_DISABLE,
  },
  location: {
    fontSize: 18,
    fontWeight: 'bold',
    color: WARNA_DISABLE,
  },

  mobilpilihan: {
    marginTop: 15,
  },

  layanan: {
    paddingHorizontal: 5,
    paddingTop: 15,
  },
  iconLayanan: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
    color: 'black',
  },
});
